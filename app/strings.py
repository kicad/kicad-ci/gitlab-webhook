# Copyright (C) Maciej Suminski <orson@orson.net.pl>
# Copyright (C) 2020 Seth Hillbrand <seth@kipro-pcb.com>
# Copyright (C) 2023 Andrew Lutsenko <anlutsenko@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may find one here:
# https://www.gnu.org/licenses/gpl-3.0.html
# or you may search the http://www.gnu.org website for the version 3 license,
# or you may write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA


from .config import KICAD_VERSION

ISSUE_MISSING_VERSION_INFO = (
    ':stop_sign: This issue is missing the required **full** version information.'
    '\n\nPlease follow the required issue template when creating a new issue. '
    'You may re-open this issue after adding the required information. '
    'Please follow this link for more information: https://go.kicad.org/bugs.'
    '\n\nIf you have encountered a build error that prevents you from compiling KiCad '
    'please add the text `build-error` to the original issue report and re-open.'
)
ISSUE_MALFORMED_VERSION_INFO = (
    ':warning: This build information does not contain the expected version data.'
    '\n\nPlease reply to this with where you downloaded your KiCad package.'
)
ISSUE_NEWER_KICAD_AVAILABLE = (
    ':stop_sign: Thank you for reporting this issue.  Since you last installed KiCad, '
    'we have released version ' + KICAD_VERSION +
    '.  Many issues have been fixed and it is possible '
    'that your issue is one of them.'
    '\n\nPlease download version ' + KICAD_VERSION +
    ' of KiCad from https://kicad.org/download/ '
    'and check that the issue still exists.  If the issue is still a problem for '
    'version ' + KICAD_VERSION + ', edit the description of this report with the updated '
    'information and re-open the ticket.'
    '\n\nWe appreciate your help in making the next version of KiCad even better than before!'
)
ISSUE_CLOSED_WITHOUT_MILESTONE = (
    ':warning: **Closed issue without a milestone assigned.**'
)
ISSUE_CLOSED_AS_DUPLICATE = (
    ':information_source: **Closed as a duplicate.**'
)
